import React, {Component} from 'react';
import {fetchRecipe, editRecipe} from '../actions/index';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import { isEmpty } from '../utils';

class RecipeEdit extends Component {

    constructor(props){
        super(props);

        this.state = {
            id: null,
            directions: null,
        }

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount(){
        const {id} = this.props.match.params;
        this.props.fetchRecipe(id);
    }

    componentDidUpdate(prevProps, prevState){
        if(!isEmpty(this.props.recipe) && this.state.id === null){
            const {id} = this.props.match.params;
            var directions = this.props.recipe.directions;
            if (!directions){directions = "";}
            this.setState({
                id: id, 
                directions: directions
            });
        }
    }

    handleChange(event){
        this.setState({directions: event.target.value});
    }

    onSubmit(event){
        event.preventDefault();
        var values = {
            directions: this.state.directions
        };
        this.props.editRecipe(this.state.id, values, () =>{
            this.props.history.push(`/recipeDetails/${this.state.id}`);
        });
    }

    render(){
        if(isEmpty(this.props.recipe) || this.state.id === null || this.state.directions === null){
            return <div>Loading...</div>
        }

        const recipe = this.props.recipe;

        return (
            <div>
                <h1 className="text-center">{recipe.name}</h1>
                <br />
                <div className="row">
                    <div className="col-sm-10"><h3>Directions</h3></div>
                </div>
                <form onSubmit={this.onSubmit}>
                    <textarea className="form-control"
                        type="text"
                        rows="10"
                        onChange={this.handleChange}
                        value={this.state.directions}
                    ></textarea>
                    <Link className="btn btn-danger btn-margin float-right" to={`/recipeDetails/${recipe.resourceId}`}>Cancel</Link>
                    <button type="submit" className="btn btn-primary btn-margin float-right">Submit</button>
                </form>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        recipe: state.recipe
    }
}

export default connect(mapStateToProps, {fetchRecipe, editRecipe})(RecipeEdit);