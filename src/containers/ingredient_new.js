import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form'; //reduxForm allows us to talk to the redux store; Field component gives us built-in event handlers for a field
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {addIngredient, fetchCategoriesByType} from '../actions';
import { isEmpty } from '../utils';

class IngredientNew extends Component {
    constructor(props){
        super(props);

        this.state = {
            id: null
        }
    }

    componentDidMount(){
        const {id} = this.props.match.params;
        this.props.fetchCategoriesByType("Ingredient");
        this.setState({id});
    }

    renderField(field){
        const {meta: {touched, error}} = field;
        const className = `form-group ${touched && error ? 'has-danger' : ''}`;

        return (
            <div className={className}>
                <label>{field.label}</label>
                <input className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">
                    {touched ? error : ''}
                </div>
            </div>
        )
    }

    renderSelectMenu(){
        return _.map(this.props.categories, category => {
            return (
                <option key={category.resourceId} value={category.resourceId}>
                        {category.name}
                </option>
            );
        });
    }

    onSubmit(values){
        values.recipe = `/recipes/${this.state.id}`;
        values.category = `/categories/${values.category}`;

        this.props.addIngredient(values, () =>{
            this.props.history.push(`/recipeDetails/${this.state.id}`);
        });
    }

    render(){

        const {handleSubmit} = this.props; // handleSubmit runs validate function on onSubmit() prior to submitting
        if(isEmpty(this.props.categories)){
            return <div>Loading...</div>;
        }
        
        return(
            <div>
                <h1 className="text-center">New Ingredient</h1>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <Field className="form-control form-control-margin" name="category" component="select">
                        <option value="" disabled>Select a Type</option>
                        {this.renderSelectMenu()}
                    </Field>
                    <Field
                        label="Name"
                        name="name"
                        component={this.renderField}
                    />
                    <Field
                        label="Quantity"
                        name="quantity"
                        component={this.renderField}
                    />
                    <Field
                        label="Measurement"
                        name="measurement"
                        component={this.renderField}
                    />
                    <Link to={`/recipeDetails/${this.state.id}`} className="btn btn-danger float-right">Cancel</Link>
                    <button type="submit" className="btn btn-primary float-right">Submit</button>
                </form>
            </div>
        );
    }
}

function validate(values){
    //console.log("in validate()");
    const errors = {};
    // validate the inputs from 'values' object
    if(!values.category){
        errors.category = "Select a Category!";
    }

    if(!values.name) { 
        errors.name = "Enter a name!";
    }
    if(!values.quantity || isNaN(values.quantity)) { 
        errors.quantity = "Enter numeric quantity!";
    }
    if(!values.measurement) { 
        errors.measurement = "Enter measurement!";
    }
    return errors;
}

function mapStateToProps(state){
    return {
        categories: state.categories
    }
}

export default reduxForm({
    validate,
    form: 'IngredientNewForm'
})(
    connect(mapStateToProps, {addIngredient, fetchCategoriesByType})(IngredientNew)
);