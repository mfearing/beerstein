import React, {Component} from 'react';
import {connect} from 'react-redux';
import {deleteIngredient} from '../actions/index';

class Ingredient extends Component {

    onDeleteClick(id, recipeId){
        this.props.deleteIngredient(id, recipeId, () => {
            //console.log('made it here, see what happens');
        });        
    }

    render(){

        var ingredient = this.props.ingredient;
        var recipeId = this.props.recipeId; 
        return (
            <li className="list-group-item">
                <div className="row">
                    <div className="col-sm-1">
                        <button className="badge badge-danger badge-pill" onClick={() => this.onDeleteClick(ingredient.resourceId, recipeId)}>
                            X
                        </button>
                    </div>
                    <div className="col-sm">
                        {ingredient.name} - {ingredient.quantity} {ingredient.measurement}
                    </div>
                </div>
            </li>
        );
    }
}

export default connect(null, {deleteIngredient})(Ingredient);