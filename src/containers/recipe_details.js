import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchRecipe, fetchIngredients, deleteRecipe} from '../actions/index';
import {isEmpty} from '../utils';
import {Link} from 'react-router-dom';

import Ingredient from './ingredient';

class RecipeDetails extends Component {

    constructor(props){
        super(props);
    }

    componentDidMount(){
        const {id} = this.props.match.params;
        this.props.fetchRecipe(id);
        this.props.fetchIngredients(id);
    }

    renderIngredients(){
        if(isEmpty(this.props.ingredients)){
            return <li className="list-group-item">Please add Ingredients!</li>;
        }

        return _.map(this.props.ingredients, ingredient => {
            return(
                <Ingredient key={ingredient.resourceId}
                    recipeId={this.props.recipe.resourceId}
                    ingredient={ingredient}
                />
            );
        })
    }

    onDeleteClick(){
        const id = this.props.recipe.resourceId;

        this.props.deleteRecipe(id, () =>{
            this.props.history.push(`/recipes`);
        });
    }

    render(){
        if(isEmpty(this.props.recipe)){
            return <div>Loading...</div>;
        }

        const recipe = this.props.recipe;

        return (
            <div>
                <h1 className="text-center">{recipe.name}</h1>
                <Link className="btn btn-sm float-right" to={`/ingredientNew/${recipe.resourceId}`}>Add Ingredient</Link>
                <div className="row">
                    <div className="col-sm-10"><h3>Ingredients </h3></div>
                </div>
                <ul className="list-group">{this.renderIngredients()}</ul>
                <br />
                <Link className="btn btn-sm float-right" to={`/recipeEdit/${recipe.resourceId}`}>Edit Directions</Link>
                <div className="row">
                    <div className="col-sm-10"><h3>Directions </h3></div>
                </div>
                <p>{recipe.directions}</p>
                
                <Link className="btn btn-primary btn-margin float-right" to={`/recipes`}>Back</Link>
                <button className="btn btn-danger btn-margin float-left" onClick={() => this.onDeleteClick()}>Delete</button>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        recipe: state.recipe,
        ingredients: state.ingredients
    }
}

export default connect(mapStateToProps, 
{
    fetchRecipe, 
    fetchIngredients,
    deleteRecipe
})(RecipeDetails);