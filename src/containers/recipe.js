import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchRecipes} from '../actions/index';
import {isEmpty} from '../utils';
import {Link} from 'react-router-dom';

class Recipe extends Component {

    constructor(props){
        super(props);

        this.state = {
            page: 0,
            totalPages: null
        }

        this.navNext = this.navNext.bind(this);
        this.navPrevious = this.navPrevious.bind(this);
    }

    componentDidMount(){
        this.props.fetchRecipes(this.state.page);
    }

    //use the below to set the component state for pagination
    componentDidUpdate(prevProps, prevState){

        if(!isEmpty(this.props.recipes) && this.state.totalPages === null){
            var totalPages = this.props.recipes.page.totalPages-1;
            this.setState({totalPages});
        }
        //update the recipes to be displayed
        if(prevState.page != this.state.page){
            this.props.fetchRecipes(this.state.page);
        }
    }


    navNext(){
        if(this.state.page < this.state.totalPages){
            var page = this.state.page+1;
            this.setState({page});
        }
    }

    navPrevious(){
        if(this.state.page > 0){
            var page = this.state.page-1;
            this.setState({page});
        }
    }

    //<button onClick={() => this.navPrevious()} className="badge badge-primary badge-pill">Previous</button>

    renderPageNavigation(){
        const page = this.props.recipes.page;

        return (
            <div className="float-right">
                <button onClick={() => {this.setState({page: 0})}} className="badge badge-primary badge-pill">First</button>
                <button onClick={() => {this.navPrevious()}} className="badge badge-primary badge-pill">Previous</button>
                [Page: {this.state.page+1} of {this.state.totalPages+1}]
                <button onClick={() => this.navNext()} className="badge badge-primary badge-pill">Next</button>
                <button onClick={() => {this.setState({page: this.state.totalPages})}} className="badge badge-primary badge-pill">Last</button>
            </div>
        );
    }

    renderRecipes(){
        const recipes = this.props.recipes._embedded.recipes;

        //return <div>Testing...</div>;

        return recipes.map((recipe) => {
            var user = recipe._embedded.user;

            return(
            <tr key={recipe.resourceId}>
                <td>{recipe.name}</td>
                <td>{recipe.category.name}</td>
                <td>{user.name}</td>
                <td><Link className="btn btn-primary float-right" to={`/recipeDetails/${recipe.resourceId}`}>Details</Link></td>
            </tr>
            );
        });
    }

    render(){
        
        if(isEmpty(this.props.recipes)){
            return <div>Loading...</div>;
        }

        return(
            <div>
                <h1 className="text-center">Recipes</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Author</th>
                            <th className="text-right">View</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRecipes()}
                    </tbody>
                </table>
                {this.renderPageNavigation()}
                <Link to={`/recipeNew`}>New Recipe</Link>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {recipes: state.recipes};
}

export default connect(mapStateToProps, {fetchRecipes})(Recipe);