import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import NavBar from './navbar';
import Home from './home';
import Recipe from'../containers/recipe'
import RecipeDetails from '../containers/recipe_details';
import IngredientNew from '../containers/ingredient_new';
import RecipeEdit from '../containers/recipe_edit';
import RecipeNew from '../containers/recipe_new';

export default class App extends Component {
    render(){
        return (
            <BrowserRouter>
                <div>
                    <NavBar />
                    <div>
                        <Switch>
                            <Route path="/ingredientNew/:id" component={IngredientNew} />
                            <Route path="/recipeDetails/:id" component={RecipeDetails} />
                            <Route path="/recipeEdit/:id" component={RecipeEdit} />
                            <Route path="/recipeNew" component={RecipeNew} />
                            <Route path="/recipes" component={Recipe} />
                            <Route path="/" component={Home} />
                        </Switch>
                    </div>
                </div>
            </BrowserRouter>
        );
    }

}