import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class NavBar extends Component {
    render() {
        return(
            <nav className="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/">Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/recipes">Recipes</Link>
                    </li>
                </ul>
            </nav>
        );
    }
}

export default NavBar;