import React, {Component} from 'react';

class Home extends Component{
    render(){
        return(
            <div>
                <h1 className="text-center">Beerstein Home</h1>
                <h6 className="text-center">A recipe application built using React.js, Spring Data REST, and MySQL</h6>
                <img className="rounded mx-auto d-block" src="http://www.tahoecoldwaterbrewery.com/wp-content/uploads/2014/10/cold-room-tahoe-cold-water-brewery.jpg" />    
                <footer className="page-footer card  btn-margin">
                    <div className="card-body text-center">
                        <h6>Michael J. Fearing</h6>
                        <h6>© Copyright 2018</h6>
                    </div>
                </footer>
            </div>
            
        );
    }
}

export default Home;