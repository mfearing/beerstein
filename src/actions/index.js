import axios from 'axios';
import { resolve } from 'path';

const ROOT_URL = `http://localhost:802/api`;

export const FETCH_RECIPES = 'FETCH_RECIPES';
export const FETCH_RECIPE = 'FETCH_RECIPE';
export const EDIT_RECIPE = 'EDIT_RECIPE';
export const DELETE_RECIPE = 'DELETE_RECIPE';
export const FETCH_INGREDIENTS = 'FETCH_INGREDIENTS';
export const CREATE_INGREDIENT = 'CREATE_INGREDIENT';
export const DELETE_INGREDIENT = 'DELETE_INGREDIENT';
export const FETCH_CATEGORIES = 'FETCH_CATEGORIES';

export function fetchRecipes(page){
    //const request = axios.get(`${ROOT_URL}/${endpoint}`, {responseType: 'json'}); //this is the ajax api call using axios, which returns a promise.
    const request = axios.get(`${ROOT_URL}/recipes?size=5&page=${page}`, {responseType: 'json'});
    return {
        type: FETCH_RECIPES,
        payload: request //because axios returns a promise for the request, the action will stop (not sent to reducers) until the request is complete
    };
}

export function fetchRecipe(id){
    const request = axios.get(`${ROOT_URL}/recipes/${id}`, {responseType: 'json'});

    return {
        type: FETCH_RECIPE,
        payload: request
    }
}

export function editRecipe(id, values, callback){
    const request = axios.patch(`${ROOT_URL}/recipes/${id}`, values, {responseType: 'json'})
        .then(() => callback());

        return {
            type: EDIT_RECIPE,
            payload: request
        }
}

export function deleteRecipe(id, callback){
    //Must include recipe id because of dependencies (?)
    const request = axios.delete(`${ROOT_URL}/recipes/${id}`)
        .then(() => callback());

    return {
        type: DELETE_RECIPE,
        payload: id
    }
}

export function fetchIngredients(id){
    const request = axios.get(`${ROOT_URL}/recipes/${id}/ingredients`, {responseType: 'json'});
    return {
        type: FETCH_INGREDIENTS,
        payload: request
    }
}

export function fetchCategoriesByType(type){
    const request = axios.get(`${ROOT_URL}/categories/search/findByType?type=${type}`);

    return {
        type: FETCH_CATEGORIES,
        payload: request
    }
}

export function addIngredient(values, callback){
    const request = axios.post(`${ROOT_URL}/ingredients`, values)
        .then(() => callback());

        return {
            type: CREATE_INGREDIENT,
            payload: request
        }
}

export function deleteIngredient(id, recipeId, callback){
    //Must include recipe id because of dependencies (?)
    const request = axios.delete(`${ROOT_URL}/recipes/${recipeId}/ingredients/${id}`)
        .then(() => callback());

    return {
        type: DELETE_INGREDIENT,
        payload: id
    }
}