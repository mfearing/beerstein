import {FETCH_INGREDIENTS, DELETE_INGREDIENT} from '../actions/index';
import _ from 'lodash';

export default function(state = {}, action){
    switch(action.type){
        case FETCH_INGREDIENTS:
            return _.mapKeys(action.payload.data._embedded.ingredients, 'resourceId');
        case DELETE_INGREDIENT:
            return _.omit(state, action.payload);
        default:
            return state;
    }
}