import {FETCH_RECIPE, DELETE_RECIPE} from '../actions/index';

export default function(state = {}, action){
    switch(action.type){
        case FETCH_RECIPE:
            return action.payload.data;
        case DELETE_RECIPE: //currently returning state as fetch will happen directly after; otherwise need to find out how to remove old entry from last fetch results
        default:
            return state;
    }
}