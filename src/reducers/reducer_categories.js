import {FETCH_CATEGORIES} from '../actions/index';
import _ from 'lodash';

export default function(state = {}, action){
    switch(action.type){
        case FETCH_CATEGORIES:
            return action.payload.data._embedded.categories; //_.mapKeys(action.payload.data._embedded.categories, 'resourceId');
        default:
            return state;
    }
}