import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import RecipesReducer from './reducer_recipes';
import RecipeReducer from './reducer_recipe';
import IngredientsReducer from './reducer_ingredients';
import CategoriesReducer from './reducer_categories';

const rootReducer = combineReducers({
    form: formReducer,
    recipes: RecipesReducer,
    recipe: RecipeReducer,
    ingredients: IngredientsReducer,
    categories: CategoriesReducer
});

export default rootReducer;
