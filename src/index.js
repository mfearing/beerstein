import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import promise from 'redux-promise';

import reducers from './reducers';
import App from './components/app';

//const client = require('./client');
const createStoreWithMiddleware = applyMiddleware(promise)(createStore); //creates redux application store

//injects react app into HTML div element with class "container"
ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <App />
    </Provider>
, document.querySelector('.container')); 